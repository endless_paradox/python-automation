#!/usr/bin/python

# all the imports for python program

import paramiko, sys, os, subprocess
from datetime import date
import time



# function to get the connection object
def getConnection(host):
    try:
        # this is used to change the private key from pem to ppk file format
        if not os.path.isfile("<PEM File>"):
            print("PEM file doesnot exists\n")
            print("Copying PEM file\n")
            subprocess.Popen("<Get your pem file to /va/tmp folder>", shell=True)
            time.sleep(10)
        k = paramiko.RSAKey.from_private_key_file("<PEM File>")
        # used to create the client which is used to ssh later
        c = paramiko.SSHClient()
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print("connecting")
        # used to connect to the remote server using hostname port username and the ppk file
        c.connect( hostname = str(host),port = 22, username = "<your user>", pkey = k )
        # if connection is created then it will say connected or else it go to except block
        print("connected")
        # returned the connection object
        return c
    except Exception as e:
        # if there is a exception in program then this will catch the exception and show the message
        print('connection to host {} is not working. Please check the server name and try again\n'.format(host))
        print('Following errors has been caught: {}'.format(e))




# use to get the imput from cli arguments
host = sys.argv
# if there is no inputs it will close the program
if(len(host)==1):
    print("\033[91m {}\033[00m" .format("No HostName Entered. Run the Command Again and enter the hostname with the command."))
    # used to exit the program
    sys.exit()

# for loop used to loop through all the hostname provided by user in the arguments
for h in host:
    # if the host name equals the python file name then don't do anything
    if(str(h) == 'ssh-user.py'):
        pass
    else:
        # print the hostname
        print("for host {}".format(h))
        # getting the connection from the above function
        connection_object = getConnection(str(h))
        # if there is no connection object then it will skip that hostname and skip to the next one
        if connection_object is None:
            continue
        # get the date for the day when the logs are created
        today = date.today()
        # All the commands that is used to move the file from logs folder to the folder we want to assign
        commands = [ "<all commands that you need to run when you want to>"]
        # for loop to execute the command one by one
        for command in commands:
            try:
                # printing the command it is going to execute
                print("Executing {}\n".format( command ))
                # executing the commands using stdin getting the output for successful out put to stdout and error in stderr.
                # stdout is not printed because there is no output for the above commands.
                stdin , stdout, stderr = connection_object.exec_command(command, get_pty = True)
                print(stdout.flush())
                print(stderr.flush())
                # printing the line for the command executed
                print("Command Executed {}\n".format( command ))
            except Exception as e:
                print(e)
                sys.exit()
        # printing all the commands executed
        print("\033[92m {}\033[00m".format("All Commands Excecuted"))
        # closing the connection object
        com = "find <directory location> -type d -ctime +10 -exec rm -rf {} +"
        stdin, stdout, stderr = connection_object.exec_command(com, get_pty= True)
        print(stdout.flush())
        connection_object.close()
