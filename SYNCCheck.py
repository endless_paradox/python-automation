import re, subprocess, time

def getNumbers(s):
    a_bool = 'UNSYNCHRONIZED' in s
    return a_bool

array1 = []
var = 0
block = ""
found = False
with open("<your file>","r") as origin_file:
    for line in origin_file.readlines():
        if found:
            block += line;
            if "<end point>" in line.strip(): break
        else:
            if "<start point>" in line.strip():
                found = True


for line in block.splitlines():
    array1.insert(var, getNumbers(line))
array1.reverse()

var1 = 0

for value in array1:
    if value == True:
        var1 +=1

if var1 == 1000:
    p = subprocess.Popen('python success1.py', shell=True)
    time.sleep(50)
    p.kill()
else:
    q = subprocess.Popen('python failure1.py', shell=True)
    time.sleep(50)
    q.kill()
