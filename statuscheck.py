import re, subprocess, time

def getNumbers(s):
    MyNumRegex = re.search(r'<your regular expression>',s)
    if MyNumRegex is None:
        return None
    else:
        return (MyNumRegex.group(1))

array1 = []
var = 0
block = ""
found = False
with open("<file>","r") as origin_file:
    for line in origin_file.readlines():
        if found:
            block += line;
            if "<End point>" in line.strip(): break
        else:
            if "<start point>" in line.strip():
                found = True

for line in block.splitlines():
    array1.insert(var, getNumbers(line))
array1.reverse()

res = []

for val in array1: 
    if val != None :
        val1 = int(val)
        res.append(val1) 
print("{}".format(res[0]))
print("{}".format(res[1]))
print("{}".format(res[2]))
print("{}".format(res[3]))
print("{}".format(res[4]))
print("{}".format(res[5]))
print("{}".format(res[6]))

if res[1] == 5000 and res[4] == 5000 and res[5] == 1000:
    p = subprocess.Popen('python success.py', shell=True)
    time.sleep(50)
    p.kill()
else:
    q = subprocess.Popen('python failure.py', shell=True)
    time.sleep(50)
    q.kill()
