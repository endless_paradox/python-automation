import paramiko

import time


ip = raw_input("Please enter IP")

name = raw_input("Please enter UserName")

password = raw_input("Please enter Password")

ssh_client = paramiko.SSHClient()

ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

ssh_client.connect(hostname=ip,username=name,password=password) #This is used to establish a connection


remote_connection = ssh_client.invoke_shell() #This helps you invoke the shell of the client machine


remote_connection.send("cli\n") #These commands are used to send command over

remote_connection.send("configure\n") #to the remote machine that you are trying to connect with



time.sleep(5)

output = remote_connection.recv(10240) #This is to recieve any output that you get on the after SSH

#connection is established

ssh_client.close() #This closes your active SSH ConnectionError
